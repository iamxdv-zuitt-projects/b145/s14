console.log("Hello from JS");

let clientName = "John Doe";
let usernumber = "09951446335"
let greetings;
let birthCity = "Manila";


console.log(clientName)
console.log(usernumber)
console.log(greetings)
console.log(birthCity)


// DATA Types

// 3 forms of Data types

// 1. Primitive -> contains only a single value:
	// ex. strings, numbers, boolean
// 2. Composite -> can contain multiple values.
	// ex. array, objects
// 3. Special
	// ex: null , undefined



// Arrays
	// Arrays are a special kind of composite data tyoe that is used to store multiple values.

//  Lets create a collection of all your subjects in the bootcamp

let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];

// Display the output of the array inside the console

console.log(bootcampSubjects);

// Rule of thumb when declaring array structures
	// --> Stroing multiples data types inside an aray is NOT RECOMMENDED.
	// An array should be a collection of data that describes a similar/single topic or subject.


	// Example : let details = [ " Keanu", "Reeves", 32, true]

// Objects
	// Objects are another special kind of comopsite data tyoe that is used to mimic or represent a real world object/item.
	//  They are used to create complex data that contains pieces of information that are relevant to each other.
	// Every individual peice of code /information is called property of an object.

	// SYNTAX:
		// let/const objectName = { key -> value 
									// propertyA: Value
									// propertyB: value	
									// }

	let cellphone = {
		brand: 'Samsung',
		model: 'A12',
		color: 'Black',
		serialNo: 'AX12002122',
		isHomeCredit: true,
		features: ["Calling", "Texting", "Ringing", "5G"],
		price: 8000
	}
// Display object inside the console

console.log(cellphone);

// Variables and Constants
 //  Variables -> are used to be store data
 // the values/info stored inside a variable can be changed or repackaged.

 let personName = "Michael";
 console.log(personName);

 // if you're going to reassign a new value for a variable, you no longer have to use the "let" again.

 personName = "Jordan";
 console.log(personName);

 // concatenating string
 	// -> join, combine, link

 	let pet = "dog";
 	console.log("this is the intial value of var: " + pet);

 	pet = "cat"
 	console.log("This is the new value of the var: " + pet);

 	// Constants 
		// Permanent, absolute, fixed
		//  the value of the assigned on a constant cannot be changed.
		//  Syntax: const desiredName = value;

		const PI = 3.14;
		console.log(PI);

		const year = 12;
		console.log(year)

		const familyName = 'Dela Cruz';
		console.log(familyName);

		// Reassigning a new value in a constant

		// familyName = 'Castillo'
		// console.log(familyName)
		// result on console will be: "Uncaught TypeError: Assignment to constant variable."


// Activity 
//  Create the following variables relating to user information:

// firstName with a value of John (a string) 
// lastName with a value of Smith (a string) 
// age with a value of 30 (an integer) 
// hobbies, an array with values of Biking, Mountain Climbing, and Swimming (all strings)


let firstName = "John"
let lastName = "Smith"
let age = 30
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];



// Create the following variables relating to user information:

// firstName with a value of John (a string) lastName with a value of Smith (a string) age with a value of 30 (an integer) hobbies, an array with values of Biking, Mountain Climbing, and Swimming (all strings)


function printUserData(firstName, lastName, age, hobbies) {
    console.log("First Name: " + firstName);
    console.log("Last Name: " + lastName);
    console.log("Age: " + age);
    console.log("Hobbies:");
    console.log(hobbies);
}

printUserData(firstName, lastName, age, hobbies);
